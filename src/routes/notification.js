const { Router } = require('express');
const { check } = require('express-validator');


const { sendEmail, getNotification } = require('../controllers/notification');
const { validarCampos } = require('../middlewares/validar-campos');
const router = Router();

router.get('/', getNotification);
router.post('/', [
    check('email', 'El correo es obligatorio').isEmail(),
    validarCampos
], sendEmail);




module.exports = router;
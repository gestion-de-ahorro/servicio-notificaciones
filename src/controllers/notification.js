const { response } = require('express');
const nodemailer = require('nodemailer');

const getNotification = (req, res = response) => {

    try {

        res.status(200).json({
            msg: `Successful notification :)`
        });

    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg: 'Error logs...'
        });
    }

}

const sendEmail = async (req, res = response) => {
    const { email } = req.body;

    try {
        const transporter = nodemailer.createTransport({
            host: 'smtp-relay.sendinblue.com',
            port: '587',
            auth: {
                user: 'alejandroperez499@gmail.com', 
                pass: 'B5gzMGyt0HECRj6d'
            }
        });
        const mailOptions = {
            from: `"Gestión De Ahorros" <alejandroperez499@gmail.com> `,
            to: `${email}`, 
            subject: 'Notificaciones Gestión De Ahorro',
            html: `<h2>Notificaciones Gestión De Ahorro</h2><br>
                    <p>Estimado reciba un cordial saludo</p>
                    <p>Ingrese a nuestro Plan de Ahorro Prormado y obtenga multiples beneficios</p>
                    <p>Su Email: ${email}</p>
            `
        };
        const send = await transporter.sendMail(mailOptions, function (err, send) {
            if (err)
                console.log(err)
            else
                ;
        });

        res.status(200).json({
            ok: true,
            msg: `Notificación enviada revice su correo/span: ${email}`
        });

    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg: 'Error logs...'
        });
    }

}

module.exports = {
    sendEmail,
    getNotification
}


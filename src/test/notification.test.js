const request = require('supertest');
const Server = require('../models/server');


const server = new Server();

describe('Send Email', function () {
    it('responds with json', function (done) {
        request(server.app)
            .get('/api/notifications')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});